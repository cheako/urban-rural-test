#![feature(async_closure)]
#![feature(duration_constants)]
#![feature(refcell_take)]

mod utils;

use wasm_bindgen::JsCast;
use wasm_bindgen_futures::JsFuture;
// use rand::{rngs::OsRng, Rng};
use js_sys::{Array, Promise};
use wasm_bindgen::prelude::*;
// use wasm_bindgen::JsCast;
// use web_sys::{
//    CanvasRenderingContext2d, Document, HtmlCanvasElement, HtmlElement, HtmlImageElement,
//    HtmlInputElement, MouseEvent, Touch, TouchEvent, Window,
// };
use web_sys::{
    console::log_1, Cache, Client, ExtendableEvent, ExtendableMessageEvent, FetchEvent,
    ServiceWorkerGlobalScope,
};

// use std::cell::RefCell;
// use std::rc::Rc;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[derive(Clone)]
pub struct Callback;

thread_local! {
    pub static GLOBAL: ServiceWorkerGlobalScope = js_sys::global() .dyn_into()
        .expect("should have a window in this context");
}

// Called by our JS entry point to run the example
#[wasm_bindgen(start)]
pub fn run() -> Result<(), JsValue> {
    utils::set_panic_hook();
    Ok(())
}

#[wasm_bindgen]
pub fn install(event: ExtendableEvent) -> Result<(), JsValue> {
    let promise = wasm_bindgen_futures::future_to_promise(async {
        use std::iter::FromIterator;

        let promise = GLOBAL.with(|g| g.caches()).unwrap().open("start_offline");

        let cache = Cache::from(JsFuture::from(promise).await?);

        JsFuture::from(
            cache.add_all_with_str_sequence(&Array::from_iter(
                [
                    "start_urban_sw.js",
                    "start_urban_sw_bg.wasm",
                    "index.html",
                    "start_start.js",
                    "start_start_bg.wasm",
                    "../icons/icon-152x152.png",
                    "../icons/icon-128x128.png",
                    "../icons/icon-512x512.png",
                    "../icons/icon-72x72.png",
                    "../icons/icon-96x96.png",
                    "../icons/icon-192x192.png",
                    "../icons/icon-384x384.png",
                    "../icons/icon-144x144.png",
                ]
                .iter()
                .map(|&s| JsValue::from(s)),
            )),
        )
        .await?;

        JsFuture::from(GLOBAL.with(|g| g.skip_waiting().unwrap())).await?;
        Ok(JsValue::UNDEFINED)
    });

    event.wait_until(&promise)
}

#[wasm_bindgen]
pub fn activate(event: ExtendableEvent) -> Result<(), JsValue> {
    let claim = Closure::once(
        Box::new(move || GLOBAL.with(|g| g.clients()).claim()) as Box<dyn FnOnce() -> Promise>
    );
    event.wait_until(claim.as_ref().unchecked_ref())?;
    claim.forget();

    Ok(())
}

#[wasm_bindgen]
pub fn fetch(event: FetchEvent) -> Result<(), JsValue> {
    let request = event.request();
    if request.method() != "GET" {
        return Ok(());
    }

    let local_event = event.clone();
    // Prevent the default, and handle the request ourselves.
    event.respond_with(&wasm_bindgen_futures::future_to_promise(async move {
        // Try to get the response from a cache.
        let promise = GLOBAL.with(|g| g.caches().unwrap().open("offline"));
        let cache = Cache::from(JsFuture::from(promise).await?);
        let cached_response = JsFuture::from(cache.match_with_request(&request)).await?;

        if cached_response.is_truthy() {
            // If we found a match in the cache, return it, but also
            // update the entry in the cache in the background.
            local_event.wait_until(&cache.add_with_request(&request))?;
            Ok(cached_response)
        } else {
            use wasm_bindgen::JsCast;
            // If we didn't find a match in the cache, use the network.
            let fetched_response: web_sys::Response =
                JsFuture::from(GLOBAL.with(|g| g.fetch_with_request(&request)))
                    .await?
                    .unchecked_into();
            JsFuture::from(cache.put_with_request(&request, &fetched_response.clone()?)).await?;
            Ok(JsValue::from(fetched_response))
        }
    }))
}

#[wasm_bindgen]
pub async fn message(event: ExtendableMessageEvent) -> Result<(), JsValue> {
    match event.data().into_serde::<commute::Request>().unwrap() {
        commute::Request::Start => event
            .source()
            .expect("EventMessage to have target")
            .dyn_into::<Client>()
            .expect("EventMessage target to be client")
            .post_message(
                &JsValue::from_serde(&commute::Response::SetText("Hello World!".to_owned()))
                    .unwrap(),
            ),
        #[allow(unreachable_patterns)]
        _ => Ok(()),
    }
}
