use serde_derive::{Deserialize, Serialize};
use std::convert::TryInto;
use wasm_bindgen::prelude::JsValue;

#[derive(Serialize, Deserialize)]
pub enum Request {
    Start,
}

impl TryInto<Request> for JsValue {
    type Error = serde_json::error::Error;

    fn try_into(self) -> Result<Request, Self::Error> {
        self.into_serde::<Request>()
    }
}

impl TryInto<JsValue> for Request {
    type Error = serde_json::error::Error;

    fn try_into(self) -> Result<JsValue, Self::Error> {
        JsValue::from_serde(&self)
    }
}

#[derive(Serialize, Deserialize)]
pub enum Response {
    SetText(String),
}

impl TryInto<Response> for JsValue {
    type Error = serde_json::error::Error;

    fn try_into(self) -> Result<Response, Self::Error> {
        self.into_serde::<Response>()
    }
}

impl TryInto<JsValue> for Response {
    type Error = serde_json::error::Error;

    fn try_into(self) -> Result<JsValue, Self::Error> {
        JsValue::from_serde(&self)
    }
}
