use wasm_bindgen::{prelude::Closure, JsCast, JsValue};
use web_sys::{
    console::{log_1, log_2},
    window as get_window, Document, MessageEvent, ServiceWorkerContainer,
    ServiceWorkerRegistration, Window,
};

thread_local! {
    pub static WINDOW: Window = get_window()
        .expect("should have a window in this context");
    static SERVICE_WORKER_CONTAINER: ServiceWorkerContainer =
        WINDOW.with(|w|w.navigator().service_worker());
    pub static SERVICE_WORKER_REGISTARTION:
        std::cell::RefCell<Option<ServiceWorkerRegistration>> =
            Default::default();
    pub static DOCUMENT: Document = WINDOW.with(|w|w.document())
        .expect("window should have a document");
}

pub async fn register_service_worker() -> Result<(), JsValue> {
    let promise = SERVICE_WORKER_CONTAINER.with(|swc| {
        let onmessage = Closure::wrap(
            Box::new(move |event: MessageEvent| log_1(&event)) as Box<dyn FnMut(MessageEvent)>
        );
        swc.set_onmessage(Some(onmessage.as_ref().unchecked_ref()));
        onmessage.forget();
        swc.register("sw.js")
    });

    match wasm_bindgen_futures::JsFuture::from(promise).await {
        Ok(r) => {
            let worker = web_sys::ServiceWorkerRegistration::from(r);
            log_2(
                &JsValue::from_str("ServiceWorker registration successful: "),
                &worker,
            );
            SERVICE_WORKER_REGISTARTION.with(|sw| sw.borrow_mut().replace(worker));
        }
        Err(e) => {
            log_2(
                &JsValue::from_str("ServiceWorker registration failed: "),
                &e,
            );
        }
    }

    Ok(())
}

pub fn send_message() -> Result<(), JsValue> {
    SERVICE_WORKER_REGISTARTION.with(|swr| {
        if let Some(swr) = swr.borrow().as_ref() {
            if let Some(sw) = swr.active() {
                sw.post_message(&JsValue::from_serde(&commute::Request::Register).unwrap())
            } else {
                Err(JsValue::UNDEFINED)
            }
        } else {
            Err(JsValue::UNDEFINED)
        }
    })
}
