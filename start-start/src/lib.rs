#![feature(async_closure)]

mod utils;

// use wasm_bindgen::JsCast;
// use rand::{rngs::OsRng, Rng};
use wasm_bindgen::prelude::*;
// use wasm_bindgen::JsCast;
// use web_sys::{
//    CanvasRenderingContext2d, Document, HtmlCanvasElement, HtmlElement, HtmlImageElement,
//    HtmlInputElement, MouseEvent, Touch, TouchEvent, Window,
// };
use web_sys::{Document, Window};

//use std::cell::RefCell;
// use std::rc::Rc;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

/*
macro_rules! console_log {
    ($($t:tt)*) => (web_sys::console::log(&Array::of1(&JsValue::from_str(&format!("{}", format_args!($($t)*))))))
}
 */

thread_local! {
    static WINDOW: Window = web_sys::window().expect("should have a window in this context");
    static DOCUMENT: Document = WINDOW.with(|w|w.document().expect("window should have a document"));
//    static BODY: HtmlElement = DOCUMENT.with(|d|d.body().expect("document should have a body"));
}

// Called by our JS entry point to run the example
#[wasm_bindgen]
pub async fn run() -> Result<(), JsValue> {
    utils::set_panic_hook();

    rural_sw::register_service_worker().await?;

    Ok(())
}
