use js_sys::{Function, Object, Promise};
use serde_derive::{Deserialize, Serialize};
use std::convert::TryInto;
use wasm_bindgen::prelude::{wasm_bindgen, JsValue};

#[derive(Serialize, Deserialize)]
pub enum Request {
    Register,
}

impl TryInto<Request> for JsValue {
    type Error = serde_json::error::Error;

    fn try_into(self) -> Result<Request, Self::Error> {
        self.into_serde::<Request>()
    }
}

impl TryInto<JsValue> for Request {
    type Error = serde_json::error::Error;

    fn try_into(self) -> Result<JsValue, Self::Error> {
        JsValue::from_serde(&self)
    }
}

#[derive(Serialize, Deserialize)]
pub enum Response {}

impl TryInto<Response> for JsValue {
    type Error = serde_json::error::Error;

    fn try_into(self) -> Result<Response, Self::Error> {
        self.into_serde::<Response>()
    }
}

impl TryInto<JsValue> for Response {
    type Error = serde_json::error::Error;

    fn try_into(self) -> Result<JsValue, Self::Error> {
        JsValue::from_serde(&self)
    }
}

#[wasm_bindgen]
extern "C" {
    pub type ReadableStreamDefaultController;

    # [ wasm_bindgen ( structural , method , getter , js_class = "ReadableStreamDefaultController" , js_name = desiredSize ) ]
    pub fn desired_size(this: &ReadableStreamDefaultController) -> i64;

    # [ wasm_bindgen ( catch , method , structural , js_class = "ReadableStreamDefaultController" , js_name = close) ]
    pub fn close(this: &ReadableStreamDefaultController) -> Result<(), JsValue>;

    # [ wasm_bindgen ( catch , method , structural , js_class = "ReadableStreamDefaultController" , js_name = enqueue) ]
    pub fn enqueue(this: &ReadableStreamDefaultController, chunk: JsValue) -> Result<(), JsValue>;

    # [ wasm_bindgen ( catch , method , structural , js_class = "ReadableStreamDefaultController" , js_name = error) ]
    pub fn error(this: &ReadableStreamDefaultController, e: JsValue) -> Result<(), JsValue>;

    pub type ReadableStreamDefaultReader;

    # [ wasm_bindgen ( structural , method , getter , js_class = "ReadableStreamDefaultReader" , js_name = closed ) ]
    pub fn closed(this: &ReadableStreamDefaultReader) -> Promise;

    # [ wasm_bindgen ( catch , method , structural , js_class = "ReadableStreamDefaultReader" , js_name = cancel) ]
    pub fn cancel(this: &ReadableStreamDefaultReader) -> Result<Promise, JsValue>;

    # [ wasm_bindgen ( catch , method , structural , js_class = "ReadableStreamDefaultReader" , js_name = cancel) ]
    pub fn cancel_with_reason(
        this: &ReadableStreamDefaultReader,
        reason: &str,
    ) -> Result<Promise, JsValue>;

    # [ wasm_bindgen ( catch , method , structural , js_class = "ReadableStreamDefaultReader" , js_name = read) ]
    pub fn read(this: &ReadableStreamDefaultReader) -> Result<Promise, JsValue>;

    # [ wasm_bindgen ( catch , method , structural , js_class = "ReadableStreamDefaultReader" , js_name = releaseLock) ]
    pub fn release_lock(this: &ReadableStreamDefaultReader) -> Result<(), JsValue>;
}

pub struct UnderlyingSource(pub Object);
impl UnderlyingSource {
    pub fn new() -> Self {
        Self(Object::new())
    }
    pub fn start(&mut self, val: &Function) -> &mut Self {
        let r = ::js_sys::Reflect::set(
            self.0.as_ref(),
            &JsValue::from("start"),
            &JsValue::from(val),
        );
        debug_assert!(
            r.is_ok(),
            "setting properties should never fail on our dictionary objects"
        );
        let _ = r;
        self
    }
    pub fn pull(&mut self, val: &Function) -> &mut Self {
        let r =
            ::js_sys::Reflect::set(self.0.as_ref(), &JsValue::from("pull"), &JsValue::from(val));
        debug_assert!(
            r.is_ok(),
            "setting properties should never fail on our dictionary objects"
        );
        let _ = r;
        self
    }
    pub fn cancel(&mut self, val: &Function) -> &mut Self {
        let r = ::js_sys::Reflect::set(
            self.0.as_ref(),
            &JsValue::from("cancel"),
            &JsValue::from(val),
        );
        debug_assert!(
            r.is_ok(),
            "setting properties should never fail on our dictionary objects"
        );
        let _ = r;
        self
    }
    pub fn type_(&mut self, val: &str) -> &mut Self {
        let r =
            ::js_sys::Reflect::set(self.0.as_ref(), &JsValue::from("type"), &JsValue::from(val));
        debug_assert!(
            r.is_ok(),
            "setting properties should never fail on our dictionary objects"
        );
        let _ = r;
        self
    }
    pub fn auto_allocate_chunk_size(&mut self, val: u32) -> &mut Self {
        let r = ::js_sys::Reflect::set(
            self.0.as_ref(),
            &JsValue::from("autoAllocateChunkSize"),
            &JsValue::from(val),
        );
        debug_assert!(
            r.is_ok(),
            "setting properties should never fail on our dictionary objects"
        );
        let _ = r;
        self
    }
}

#[wasm_bindgen]
extern "C" {
    pub type ReadableStream;

    #[wasm_bindgen(constructor)]
    pub fn new(underlying_source: &Object) -> ReadableStream;

    # [ wasm_bindgen ( catch , method , structural , js_class = "ReadableStream" , js_name = getReader) ]
    pub fn get_reader(this: &ReadableStream) -> Result<ReadableStreamDefaultReader, JsValue>;

    pub type ReadableStreamDefaultReaderResult;

    # [ wasm_bindgen ( structural , method , getter , js_class = "ReadableStreamDefaultReaderResult" , js_name = done ) ]
    pub fn done(this: &ReadableStreamDefaultReaderResult) -> bool;

    # [ wasm_bindgen ( structural , method , getter , js_class = "ReadableStreamDefaultReaderResult" , js_name = value ) ]
    pub fn value(this: &ReadableStreamDefaultReaderResult) -> JsValue;
}
