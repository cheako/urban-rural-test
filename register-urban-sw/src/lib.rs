#![feature(async_closure)]
#![feature(duration_constants)]
#![feature(refcell_take)]

mod utils;

use wasm_bindgen::JsCast;
use wasm_bindgen_futures::JsFuture;
// use rand::{rngs::OsRng, Rng};
use js_sys::{Array, Promise, Uint8Array};
use wasm_bindgen::prelude::*;
// use wasm_bindgen::JsCast;
// use web_sys::{
//    CanvasRenderingContext2d, Document, HtmlCanvasElement, HtmlElement, HtmlImageElement,
//    HtmlInputElement, MouseEvent, Touch, TouchEvent, Window,
// };
use web_sys::{
    console::log_1, Cache, ExtendableEvent, FetchEvent, MessageEvent, Response, ResponseInit,
    ServiceWorkerGlobalScope,
};

// use std::cell::RefCell;
// use std::rc::Rc;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = Date, js_name = now)]
    fn date_now() -> f64;
}

thread_local! {
    pub static GLOBAL: ServiceWorkerGlobalScope = js_sys::global() .dyn_into()
        .expect("should have a window in this context");
}

// Called by our JS entry point to run the example
#[wasm_bindgen(start)]
pub fn run() -> Result<(), JsValue> {
    utils::set_panic_hook();

    Ok(())
}

#[wasm_bindgen]
pub fn install(event: ExtendableEvent) -> Result<(), JsValue> {
    let promise = wasm_bindgen_futures::future_to_promise(async {
        use std::iter::FromIterator;

        let promise = GLOBAL
            .with(|g| g.caches())
            .unwrap()
            .open("register_offline");

        let cache = Cache::from(JsFuture::from(promise).await?);

        JsFuture::from(
            cache.add_all_with_str_sequence(&Array::from_iter(
                [
                    "register_urban_sw.js",
                    "register_urban_sw_bg.wasm",
                    "index.html",
                    "register_start.js",
                    "register_start_bg.wasm",
                    "../icons/icon-152x152.png",
                    "../icons/icon-128x128.png",
                    "../icons/icon-512x512.png",
                    "../icons/icon-72x72.png",
                    "../icons/icon-96x96.png",
                    "../icons/icon-192x192.png",
                    "../icons/icon-384x384.png",
                    "../icons/icon-144x144.png",
                ]
                .iter()
                .map(|&s| JsValue::from(s)),
            )),
        )
        .await?;

        JsFuture::from(GLOBAL.with(|g| g.skip_waiting().unwrap())).await?;
        Ok(JsValue::UNDEFINED)
    });

    event.wait_until(&promise)
}

#[wasm_bindgen]
pub fn activate(event: ExtendableEvent) -> Result<(), JsValue> {
    let claim = Closure::once(
        Box::new(move || GLOBAL.with(|g| g.clients()).claim()) as Box<dyn FnOnce() -> Promise>
    );
    event.wait_until(claim.as_ref().unchecked_ref())?;
    claim.forget();

    Ok(())
}

#[wasm_bindgen]
pub fn fetch(event: FetchEvent) -> Result<(), JsValue> {
    let request = event.request();
    if request.method() != "GET" {
        return Ok(());
    }

    let local_event = event.clone();
    // Prevent the default, and handle the request ourselves.
    event.respond_with(&wasm_bindgen_futures::future_to_promise(async move {
        // Try to get the response from a cache.
        let promise = GLOBAL.with(|g| g.caches().unwrap().open("offline"));
        let cache = Cache::from(JsFuture::from(promise).await?);
        let cached_response = JsFuture::from(cache.match_with_request(&request)).await?;

        let response = if cached_response.is_truthy() {
            // If we found a match in the cache, return it, but also
            // update the entry in the cache in the background.
            local_event.wait_until(&cache.add_with_request(&request))?;
            cached_response
                .dyn_into::<Response>()
                .expect("cached_response to be Response")
        } else {
            use wasm_bindgen::JsCast;
            // If we didn't find a match in the cache, use the network.
            let fetched_response: Response =
                JsFuture::from(GLOBAL.with(|g| g.fetch_with_request(&request)))
                    .await?
                    .unchecked_into();
            JsFuture::from(cache.put_with_request(&request, &fetched_response.clone()?)).await?;
            fetched_response
        };

        if let Ok(Some(content_type)) = response.headers().get("content-type") {
            log_1(&content_type.clone().into());
            if content_type.starts_with("text/html") {if let Some(body) = response.body() {
            let mut response_init = ResponseInit::new();
            response_init.headers(&response.headers());
            response_init.status(response.status());
            response_init.status_text(&response.status_text());

            let reader = body
                .unchecked_into::<commute::ReadableStream>()
                .get_reader()
                .expect("body stream to have reader")
                .unchecked_into::<commute::ReadableStreamDefaultReader>();
            let readable_stream = Closure::once(Box::new(
                move |controller: commute::ReadableStreamDefaultController| {
                    wasm_bindgen_futures::future_to_promise(async move {
                        let mut r: Option<usize> = None;
                        loop {
                            let read = JsFuture::from(reader.read()?)
                                .await?
                                .unchecked_into::<commute::ReadableStreamDefaultReaderResult>(
                            );
                            if read.done() {
                                log_1(&"done".into());
                                controller.close()?;
                                return Ok(JsValue::UNDEFINED);
                            }
                            let value = read.value();
                            if let Some(v) = value.dyn_ref::<Uint8Array>() {
                                const MATCH: &str = "\"#%JSON%#\"";
                                let vec = v.to_vec();
                                let mut iter = MATCH
                                    .bytes()
                                    .take(r.unwrap_or_default())
                                    .chain(vec.into_iter())
                                    .enumerate()
                                    .peekable();
                                let vec = v.to_vec();
                                match { let aloop = Array::new();let ret = 'a: loop {
                                    if let Some((i, rhs)) = iter.next() {
                                        let mut match_ = MATCH.bytes();
                                        let bloop = Array::new();
                                        if let Some(lhs) = match_.next() {
                                            let ret = if lhs == rhs {
                                                bloop.push(&"First match".into());
                                                loop {
                                                    if let Some(lhs) = match_.next() {
                                                        if let Some((_, rhs)) = iter.peek() {
                                                            bloop.push(&"Have rhs".into());
                                                            if lhs != *rhs {
                                                                bloop.push(&format!("{}: continue 'a", *rhs as char).into());
                                                                aloop.push(&bloop);
                                                                continue 'a;
                                                            }
                                                        } else {
                                                                bloop.push(&format!("end of content with partial match: {} ({}, {})", MATCH.len() - match_.count(), i ,r.unwrap_or_default() ).into());
                                                                break 'a Some(
                                                                    i - r.unwrap_or_default(),
                                                                );
                                                        }
                                                    } else {
                                                                bloop.push(&format!("found match: ({} , {})", i, r.unwrap_or_default()).into());
                                                        break 'a Some(i - r.unwrap_or_default());
                                                    }
                                                }
                                            };
                                            if bloop.length() !=   0 {                  aloop.push(&bloop);
                                                               }; ret
                                        } else {
                                            unreachable!("MATCH should have at least one byte")
                                        }
                                    } else {
                                        break None;
                                    }
                                }; log_1(&aloop); ret } {
                                    Some(x) => {
                                        const HELLO_WORLD: &str = "{ 'a': 'Hello World!' }";
                                        let end_of_match = x + MATCH.len();
                                        let hello_world =
                                            Uint8Array::new_with_length(HELLO_WORLD.len() as _);
                                        HELLO_WORLD.bytes().enumerate().for_each(|(i, v)| {
                                            hello_world.set_index(i as _, v as _)
                                        });
                                        if let Some(intro) = r {
                                            let remaning =
                                                (intro + vec.len()) as i32 - end_of_match as i32;
                                            if remaning > 0 {
                                                log_1(&"have match with fragment.".into());
                                                if x != 0 {
                                                    log_1(&"didn't match at the start.".into());
                                                    let partial_match =
                                                        Uint8Array::new_with_length(intro as _);
                                                    MATCH.bytes().take(intro).enumerate().for_each(
                                                        |(i, v)| {
                                                            partial_match.set_index(i as _, v as _)
                                                        },
                                                    );
                                                    controller
                                                        .enqueue(JsValue::from(partial_match))?;
                                                    controller.enqueue(JsValue::from(
                                                        v.slice(0, (x - intro) as _),
                                                    ))?;
                                                }
                                                controller.enqueue(JsValue::from(hello_world))?;
                                                controller.enqueue(JsValue::from(v.slice(
                                                    (end_of_match - intro) as _,
                                                    vec.len() as _,
                                                )))?;
                                            } else {
                                                r = Some(-remaning as _);
                                            }
                                        } else {
                                            let remaning = vec.len() as i32 - end_of_match as i32;
                                            if remaning > 0 {
                                                log_1(&"have match without fragment.".into());
                                                controller
                                                    .enqueue(JsValue::from(v.slice(0, x as _)))?;
                                                let hello_world = Uint8Array::new_with_length(
                                                    HELLO_WORLD.len() as _,
                                                );
                                                HELLO_WORLD.bytes().enumerate().for_each(
                                                    |(i, v)| hello_world.set_index(i as _, v as _),
                                                );
                                                controller.enqueue(JsValue::from(hello_world))?;
                                                controller.enqueue(JsValue::from(
                                                    v.slice(end_of_match as _, vec.len() as _),
                                                ))?;
                                            } else {
                                                r = Some(-remaning as _);
                                            }
                                        }
                                    }
                                    None => {
                                        if let Some(r) = r {
                                            log_1(&"no match with fragment.".into());
                                            let array =
                                                Uint8Array::new_with_length((r + vec.len()) as _);
                                            MATCH
                                                .bytes()
                                                .take(r)
                                                .chain(vec.into_iter())
                                                .enumerate()
                                                .for_each(|(i, v)| array.set_index(i as _, v as _));
                                            controller.enqueue(JsValue::from(array))?
                                        } else {
                                            log_1(&"no match without fragment.".into());
                                            controller.enqueue(value)?
                                        }
                                    }
                                }
                            } else {
                                log_1(&"didn't have Uint8Array.".into());
                                controller.enqueue(value)?
                            }
                        }
                    })
                },
            )
                as Box<dyn FnOnce(commute::ReadableStreamDefaultController) -> Promise>);
            let response_body = commute::ReadableStream::new(
                &commute::UnderlyingSource::new()
                    .start(readable_stream.as_ref().unchecked_ref())
                    .0,
            );
            readable_stream.forget();

            Response::new_with_opt_readable_stream_and_init(
                Some(response_body.unchecked_ref()),
                &response_init,
            )
        } else {
            Ok(response)
        }} else {
            Ok(response)
        }}else {
            Ok(response)
        }
        .map(|v| JsValue::from(v))
    }))
}

#[wasm_bindgen]
pub async fn message(event: MessageEvent) -> Result<(), JsValue> {
    match event.data().into_serde::<commute::Request>().unwrap() {
        commute::Request::Register => log_1(&event),
        #[allow(unreachable_patterns)]
        _ => {}
    }
    Ok(())
}
